package io.gitee.opabinia.binlog4j.springboot.starter.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.springframework.stereotype.Component;

@Documented
@Component
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface BinlogSubscriber {

  /**
   * 客户端
   * <p>
   * 在 spring boot 下, 需指定将该 handler 实例, 注册到那个 client 客户端
   */
  String clientName();

}
