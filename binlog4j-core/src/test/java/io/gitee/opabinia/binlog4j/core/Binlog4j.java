package io.gitee.opabinia.binlog4j.core;

import io.gitee.opabinia.binlog4j.core.config.RedisConfig;
import io.gitee.opabinia.binlog4j.core.enums.BinlogClientMode;
import io.gitee.opabinia.binlog4j.core.handler.SuperBinlogEventHandler;
import java.io.Serializable;

public class Binlog4j {

  public static void main(String[] args) {

    RedisConfig redisConfig = new RedisConfig();
    redisConfig.setHost("127.0.0.1");
    redisConfig.setPort(6379);

    // Binlog 客户端【配置】 非 Spring
    BinlogClientConfig clientConfig = new BinlogClientConfig();
    clientConfig.setHost("127.0.0.1");
    clientConfig.setPort(3306);
    clientConfig.setUsername("root");
    clientConfig.setPassword("root");
    clientConfig.setServerId(1990);
    clientConfig.setRedisConfig(redisConfig); // 依赖中间件（支撑 持久化模式 与 高可用 集群）
    clientConfig.setPersistence(true); // 持久化模式
    // 首次启动
    clientConfig.setInaugural(true);
    clientConfig.setBinlogFilename("LAPTOP-G4CLDSG3-bin.000224");
    clientConfig.setBinlogPosition(6982L);
    // clientConfig.setMode(BinlogClientMode.CLUSTER); // 集群模式
    clientConfig.setMode(BinlogClientMode.STANDALONE); // 单机模式

    BinlogClient<Serializable> binlogClient = new BinlogClient<>(clientConfig);

    binlogClient.registerEventHandler(new SuperBinlogEventHandler());

    binlogClient.connect();
  }
}
