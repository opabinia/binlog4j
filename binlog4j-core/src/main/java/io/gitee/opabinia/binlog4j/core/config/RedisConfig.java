package io.gitee.opabinia.binlog4j.core.config;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class RedisConfig {

    private String host;

    private int port = 6379;

    private String password;

    private int database;

}
