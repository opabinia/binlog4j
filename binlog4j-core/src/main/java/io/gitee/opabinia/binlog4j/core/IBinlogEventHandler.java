package io.gitee.opabinia.binlog4j.core;

/**
 * Binlog 事件处理器接口
 *
 * @author 就眠儀式
 */
public interface IBinlogEventHandler<T> {

  /**
   * 插入
   *
   * @param event 事件详情
   */
  void onInsert(BinlogEvent<T> event);

  /**
   * 修改
   *
   * @param event 事件详情
   */
  void onUpdate(BinlogEvent<T> event);

  /**
   * 删除
   *
   * @param event 事件详情
   */
  void onDelete(BinlogEvent<T> event);


  /**
   * 前置
   * <p>
   * 控制该 handler 是否处理当前 Binlog Event
   */
  boolean isHandle(String database, String table);

}
