package io.gitee.opabinia.binlog4j.core;

import com.zaxxer.hikari.HikariConfig;
import io.gitee.opabinia.binlog4j.core.config.RedisConfig;
import io.gitee.opabinia.binlog4j.core.enums.BinlogClientMode;
import io.gitee.opabinia.binlog4j.core.position.BinlogPositionHandler;
import io.gitee.opabinia.binlog4j.core.utils.MD5Utils;
import java.util.concurrent.TimeUnit;
import lombok.Data;

/**
 * Binlog Client 配置
 *
 * @author 就眠儀式
 */
@Data
public class BinlogClientConfig {

  /**
   * 账户
   */
  private String username;

  /**
   * 密码
   */
  private String password;

  /**
   * 地址
   */
  private String host;

  /**
   * 端口
   */
  private int port = 3306;

  /**
   * 时间偏移量
   */
  private long timeOffset = 0;

  /**
   * 客户端编号 (不同的集群)
   */
  private long serverId;

  /**
   * 是否保持连接
   */
  private boolean keepAlive = true;

  /**
   * 是否是首次启动
   */
  private boolean inaugural = false;

  /**
   * 保持连接时间
   */
  private long keepAliveInterval = TimeUnit.MINUTES.toMillis(1L);

  /**
   * 连接超时时间
   */
  private long connectTimeout = TimeUnit.SECONDS.toMillis(3L);

  /**
   * 发送心跳包时间间隔
   */
  private long heartbeatInterval = TimeUnit.SECONDS.toMillis(6L);

  /**
   * “分布式” “记忆读取”
   * <p>
   * 依赖的 Redis 中间件配置
   */
  private RedisConfig redisConfig;

  /**
   * 读取记忆
   */
  private boolean persistence = false;

  /**
   * 严格模式
   * <p>
   * 性能与健壮性的平衡
   */
  private boolean strict = true;

  /**
   * 部署模式
   */
  private BinlogClientMode mode = BinlogClientMode.STANDALONE;

  /**
   * 持久化 PositionHandler 实现 (优先级 > RedisConfig)
   */
  private BinlogPositionHandler positionHandler;

  private HikariConfig hikariConfig;

  /**
   * bin log 文件名称, 如果指定，需要指定正确文件名，否则程序无法正常运行
   */
  private String binlogFilename;

  /**
   * bin log 读取位置, 默认不设置，从当前位置开始读取，如果指定, 需要指定正确位置，否则程序无法正确执行
   */
  private Long binlogPosition;

  public String getKey() {
    return MD5Utils.encrypt(this.host + ":" + this.port + ":" + this.serverId);
  }
}
