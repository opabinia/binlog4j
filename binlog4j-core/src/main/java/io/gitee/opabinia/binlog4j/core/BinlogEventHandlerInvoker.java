package io.gitee.opabinia.binlog4j.core;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.gitee.opabinia.binlog4j.core.exception.Binlog4jException;
import io.gitee.opabinia.binlog4j.core.meta.ColumnMetadata;
import io.gitee.opabinia.binlog4j.core.utils.ClassUtils;
import io.gitee.opabinia.binlog4j.core.utils.JDBCUtils;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.util.BitSet;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.Data;

/**
 * Binlog 处理器包装类
 *
 * @author 就眠儀式
 */
@Data
public class BinlogEventHandlerInvoker<T> {

  private Map<String, List<ColumnMetadata>> columnMetadataMap = new HashMap<>();

  private IBinlogEventHandler<T> eventHandler;

  private BinlogClientConfig clientConfig;

  private Class<T> genericClass;

  public void invokeInsert(String databaseName, String tableName, List<Serializable[]> data) {
    if (eventHandler.isHandle(databaseName, tableName)) {
      List<ColumnMetadata> columns = getColumns(databaseName, tableName);
      BinlogEvent<T> binlogEvent = createBinlogEvent(databaseName, tableName);
      data.forEach(row -> {
        binlogEvent.setData(toEntity(columns, row));
        eventHandler.onInsert(binlogEvent);
      });
    }
  }

  public void invokeUpdate(String databaseName, String tableName,
      List<Map.Entry<Serializable[], Serializable[]>> data) {
    if (eventHandler.isHandle(databaseName, tableName)) {
      List<ColumnMetadata> columns = getColumns(databaseName, tableName);
      BinlogEvent<T> binlogEvent = createBinlogEvent(databaseName, tableName);
      data.forEach(row -> {
        binlogEvent.setData(toEntity(columns, row.getValue()));
        binlogEvent.setOriginalData(toEntity(columns, row.getKey()));
        eventHandler.onUpdate(binlogEvent);
      });
    }
  }

  public void invokeDelete(String databaseName, String tableName, List<Serializable[]> data) {
    if (eventHandler.isHandle(databaseName, tableName)) {
      List<ColumnMetadata> columns = getColumns(databaseName, tableName);
      BinlogEvent<T> binlogEvent = createBinlogEvent(databaseName, tableName);
      data.forEach(row -> {
        binlogEvent.setData(toEntity(columns, row));
        eventHandler.onDelete(binlogEvent);
      });
    }
  }

  private BinlogEvent<T> createBinlogEvent(String databaseName, String tableName) {
    BinlogEvent<T> binlogEvent = new BinlogEvent<>();
    binlogEvent.setDatabase(databaseName);
    binlogEvent.setTable(tableName);
    binlogEvent.setTimestamp(System.currentTimeMillis());
    return binlogEvent;
  }

  public List<ColumnMetadata> getColumns(String databaseName, String tableName) {
    String tableSchema = String.format("%s.%s", databaseName, tableName);
    List<ColumnMetadata> columns = columnMetadataMap.get(tableSchema);
    if (columns == null || clientConfig.isStrict()) {
      columns = JDBCUtils.getColumns(clientConfig, databaseName, tableName);
      columnMetadataMap.put(tableSchema, columns);
    }
    return columns;
  }

  @SuppressWarnings("unchecked")
  public T toEntity(List<ColumnMetadata> columns, Serializable[] data) {
    if (columns == null || data == null || columns.size() != data.length) {
      throw new IllegalArgumentException(
          "Columns and data must not be null and have matching lengths.");
    }

    Map<String, Object> obj = new HashMap<>(columns.size());
    for (int i = 0; i < data.length; i++) {
      ColumnMetadata column = columns.get(i);
      Serializable fieldValue = data[i];
      fieldValue = convertFieldValue(fieldValue, column);
      obj.put(toCamelCase(column.getColumnName()), fieldValue);
    }

    if (genericClass != null) {
      try {
        final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return objectMapper.convertValue(obj, genericClass);
      } catch (Exception e) {
        throw new Binlog4jException("Error casting object to generic class", e);
      }
    }
    return (T) obj;
  }

  private Serializable convertFieldValue(Serializable fieldValue, ColumnMetadata column) {
    if (fieldValue instanceof java.util.Date) {
      return new Date(((Date) fieldValue).getTime() + clientConfig.getTimeOffset());
    } else if (fieldValue instanceof byte[]) {
      return convertByteArray(fieldValue, column);
    } else if (fieldValue instanceof BitSet) {
      return convertBitSet(fieldValue, column);
    }
    return fieldValue;
  }

  private Serializable convertByteArray(Serializable fieldValue, ColumnMetadata column) {
    if (genericClass != null) {
      try {
        Field field = ClassUtils.getDeclaredField(genericClass, column.getColumnName());
        if (field != null && field.getType() == String.class) {
          return new String((byte[]) fieldValue, StandardCharsets.UTF_8);
        }
      } catch (Exception e) {
        throw new Binlog4jException("Error converting byte array", e);
      }
    }
    return fieldValue;
  }

  private Serializable convertBitSet(Serializable fieldValue, ColumnMetadata column) {
    if (genericClass != null) {
      try {
        Field field = ClassUtils.getDeclaredField(genericClass, column.getColumnName());
        if (field != null && (field.getType() == Boolean.class
            || field.getType() == boolean.class)) {
          return !((BitSet) fieldValue).isEmpty();
        }
      } catch (Exception e) {
        throw new Binlog4jException("Error converting BitSet", e);
      }
    }
    return fieldValue;
  }

  @SuppressWarnings("unchecked")
  public void setEventHandler(IBinlogEventHandler<T> eventHandler) {
    this.eventHandler = eventHandler;
    this.genericClass = (Class<T>) ClassUtils.getGenericType(eventHandler.getClass());
  }

  private String toCamelCase(String text) {
    boolean shouldConvertNextCharToLower = true;
    StringBuilder builder = new StringBuilder();
    for (int i = 0; i < text.length(); i++) {
      char currentChar = text.charAt(i);
      if (currentChar == '_') {
        shouldConvertNextCharToLower = false;
      } else if (shouldConvertNextCharToLower) {
        builder.append(Character.toLowerCase(currentChar));
      } else {
        builder.append(Character.toUpperCase(currentChar));
        shouldConvertNextCharToLower = true;
      }
    }
    return builder.toString();
  }
}
