package io.gitee.opabinia.binlog4j.core;

import lombok.Data;

/**
 * Binlog 事件详情
 *
 * @author 就眠儀式
 */
@Data
public class BinlogEvent<T> {

  /**
   * 来源 Table
   */
  private String table;

  /**
   * 来源 Database
   */
  private String database;

  /**
   * 新数据
   */
  private T data;

  /**
   * 原数据
   */
  private T originalData;

  /**
   * 时间戳
   */
  private Long timestamp;
}
