package io.gitee.opabinia.binlog4j.core.utils;

import io.gitee.opabinia.binlog4j.core.exception.Binlog4jException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * MD5 Utils
 *
 * @author 就眠儀式
 */
public class MD5Utils {

  private MD5Utils() {

  }

  public static String encrypt(String input) {
    try {
      MessageDigest md = MessageDigest.getInstance("MD5");
      byte[] messageDigest = md.digest(input.getBytes());

      StringBuilder hexString = new StringBuilder();
      for (byte b : messageDigest) {
        String hex = Integer.toHexString(0xFF & b);
        if (hex.length() == 1) {
          hexString.append('0');
        }
        hexString.append(hex);
      }
      return hexString.toString();
    } catch (NoSuchAlgorithmException e) {
      throw new Binlog4jException(e);
    }
  }
}
