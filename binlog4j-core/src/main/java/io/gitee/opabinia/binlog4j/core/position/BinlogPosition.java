package io.gitee.opabinia.binlog4j.core.position;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class BinlogPosition {

  private Long serverId;

  private Long position;

  private String filename;

}
