package io.gitee.opabinia.binlog4j.core.position;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.gitee.opabinia.binlog4j.core.config.RedisConfig;
import io.gitee.opabinia.binlog4j.core.exception.Binlog4jException;
import lombok.extern.slf4j.Slf4j;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;


@Slf4j
public class RedisBinlogPositionHandler implements BinlogPositionHandler {

  private final RedissonClient redissonClient;

  public RedisBinlogPositionHandler(RedisConfig redisConfig) {
    Config config = new Config();
    config.useSingleServer()
        .setAddress("redis://" + redisConfig.getHost() + ":" + redisConfig.getPort())
        .setPassword(redisConfig.getPassword())
        .setDatabase(redisConfig.getDatabase());
    config.setLockWatchdogTimeout(10000L);
    this.redissonClient = Redisson.create(config);
  }

  @Override
  public BinlogPosition loadPosition(Long serverId) {

    try {
      final Object value = redissonClient.getBucket(serverId.toString()).get();
      if (value != null) {
        return new ObjectMapper().readValue(value.toString(), BinlogPosition.class);
      }
    } catch (Exception e) {
      throw new Binlog4jException("Unable to connect to Redis host.");
    }
    return null;
  }

  @Override
  public void savePosition(BinlogPosition position) {
    try {
      redissonClient.getBucket(position.getServerId().toString())
          .set(new ObjectMapper().writeValueAsString(position));
    } catch (JsonProcessingException e) {
      throw new Binlog4jException(e);
    }

  }
}
