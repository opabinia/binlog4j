package io.gitee.opabinia.binlog4j.core.enums;

public enum BinlogClientMode {

    STANDALONE,

    CLUSTER
}
