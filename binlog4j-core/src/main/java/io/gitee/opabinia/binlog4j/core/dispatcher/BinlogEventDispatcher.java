package io.gitee.opabinia.binlog4j.core.dispatcher;

import io.gitee.opabinia.binlog4j.core.position.BinlogPosition;
import io.gitee.opabinia.binlog4j.core.BinlogClientConfig;
import io.gitee.opabinia.binlog4j.core.BinlogEventHandlerInvoker;
import io.gitee.opabinia.binlog4j.core.exception.Binlog4jException;
import io.gitee.opabinia.binlog4j.core.position.BinlogPositionHandler;
import com.github.shyiko.mysql.binlog.BinaryLogClient;
import com.github.shyiko.mysql.binlog.event.DeleteRowsEventData;
import com.github.shyiko.mysql.binlog.event.Event;
import com.github.shyiko.mysql.binlog.event.EventData;
import com.github.shyiko.mysql.binlog.event.EventHeaderV4;
import com.github.shyiko.mysql.binlog.event.EventType;
import com.github.shyiko.mysql.binlog.event.RotateEventData;
import com.github.shyiko.mysql.binlog.event.TableMapEventData;
import com.github.shyiko.mysql.binlog.event.UpdateRowsEventData;
import com.github.shyiko.mysql.binlog.event.WriteRowsEventData;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class BinlogEventDispatcher<T> implements BinaryLogClient.EventListener {

  private final Map<Long, TableMapEventData> tableMap = new HashMap<>();

  private final Map<String, BinlogEventHandlerInvoker<T>> eventHandlerMap;

  private final BinlogClientConfig clientConfig;

  private final BinlogPositionHandler binlogPositionHandler;

  public BinlogEventDispatcher(BinlogClientConfig clientConfig,
      BinlogPositionHandler positionHandler,
      Map<String, BinlogEventHandlerInvoker<T>> eventHandlerMap) {
    this.clientConfig = clientConfig;
    this.eventHandlerMap = eventHandlerMap;
    this.binlogPositionHandler = positionHandler;
  }

  @Override
  public void onEvent(Event event) {
    try {
      EventHeaderV4 headerV4 = event.getHeader();
      if (headerV4 == null) {
        // 处理无效header情况
        return;
      }
      EventType eventType = headerV4.getEventType();

      if (eventType == EventType.TABLE_MAP) {
        TableMapEventData eventData = event.getData();
        if (eventData != null) { // 校验数据有效性
          tableMap.put(eventData.getTableId(), eventData);
        }
      } else {
        processRowMutationEvent(eventType, event);
      }

      if (clientConfig.isPersistence()) {
        processBinlogPosition(eventType, event, headerV4);
      }
    } catch (Exception e) {
      throw new Binlog4jException(e);
    }
  }

  private void processRowMutationEvent(EventType eventType, Event event) {
    if (EventType.isRowMutation(eventType)) {
      RowMutationEventData rowMutationEventData = new RowMutationEventData(event.getData());
      TableMapEventData tableMapEventData = tableMap.get(rowMutationEventData.getTableId());
      if (tableMapEventData != null) {
        String database = tableMapEventData.getDatabase();
        String table = tableMapEventData.getTable();
        eventHandlerMap.forEach((handlerKey, eventHandler) -> {
          if (EventType.isUpdate(eventType)) {
            eventHandler.invokeUpdate(database, table, rowMutationEventData.getUpdateRows());
          } else if (EventType.isDelete(eventType)) {
            eventHandler.invokeDelete(database, table, rowMutationEventData.getDeleteRows());
          } else if (EventType.isWrite(eventType)) {
            eventHandler.invokeInsert(database, table, rowMutationEventData.getInsertRows());
          } else {
            // 处理未知事件类型
            handleUnknownEventType(eventType);
          }
        });
      }
    }
  }

  private void processBinlogPosition(EventType eventType, Event event, EventHeaderV4 headerV4) {
    if (binlogPositionHandler != null && eventType != EventType.FORMAT_DESCRIPTION) {
      BinlogPosition binlogPosition = EventType.ROTATE == eventType
          ? createBinlogPositionFromRotateEventData(event.getData())
          : loadAndUpdateBinlogPosition(headerV4);
      binlogPositionHandler.savePosition(binlogPosition);
    }
  }

  private BinlogPosition createBinlogPositionFromRotateEventData(EventData eventData) {
    // 详细实现省略，基于RotateEventData创建BinlogPosition
    BinlogPosition binlogPosition = new BinlogPosition();
    binlogPosition.setServerId(clientConfig.getServerId());
    binlogPosition.setFilename(((RotateEventData) eventData).getBinlogFilename());
    binlogPosition.setPosition(((RotateEventData) eventData).getBinlogPosition());

    return binlogPosition;
  }

  private BinlogPosition loadAndUpdateBinlogPosition(EventHeaderV4 headerV4) {
    BinlogPosition binlogPosition = binlogPositionHandler.loadPosition(clientConfig.getServerId());
    if (binlogPosition != null) {
      binlogPosition.setPosition(headerV4.getNextPosition());
    }
    return binlogPosition;
  }

  private void handleUnknownEventType(EventType eventType) {
    // 处理未知事件类型的逻辑，如记录日志
    log.info(eventType.toString());
  }

  @Data
  public static class RowMutationEventData {

    private long tableId;

    private List<Serializable[]> insertRows;

    private List<Serializable[]> deleteRows;

    private List<Map.Entry<Serializable[], Serializable[]>> updateRows;

    public RowMutationEventData(EventData eventData) {

      if (eventData instanceof UpdateRowsEventData) {
        UpdateRowsEventData updateRowsEventData = (UpdateRowsEventData) eventData;
        this.tableId = updateRowsEventData.getTableId();
        this.updateRows = updateRowsEventData.getRows();
        return;
      }

      if (eventData instanceof WriteRowsEventData) {
        WriteRowsEventData writeRowsEventData = (WriteRowsEventData) eventData;
        this.tableId = writeRowsEventData.getTableId();
        this.insertRows = writeRowsEventData.getRows();
        return;
      }

      if (eventData instanceof DeleteRowsEventData) {
        DeleteRowsEventData deleteRowsEventData = (DeleteRowsEventData) eventData;
        this.tableId = deleteRowsEventData.getTableId();
        this.deleteRows = deleteRowsEventData.getRows();
      }
    }
  }
}
