package io.gitee.opabinia.binlog4j.core;

import com.github.shyiko.mysql.binlog.BinaryLogClient;
import io.gitee.opabinia.binlog4j.core.config.RedisConfig;
import io.gitee.opabinia.binlog4j.core.dispatcher.BinlogEventDispatcher;
import io.gitee.opabinia.binlog4j.core.enums.BinlogClientMode;
import io.gitee.opabinia.binlog4j.core.exception.Binlog4jException;
import io.gitee.opabinia.binlog4j.core.position.BinlogPosition;
import io.gitee.opabinia.binlog4j.core.position.BinlogPositionHandler;
import io.gitee.opabinia.binlog4j.core.position.RedisBinlogPositionHandler;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import lombok.extern.slf4j.Slf4j;
import org.redisson.Redisson;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.redisson.config.SingleServerConfig;

/**
 * Binlog 客户端
 *
 * @author 就眠儀式
 */
@Slf4j
public class BinlogClient<T> implements IBinlogClient<T> {

  private final BinlogClientConfig clientConfig;

  private BinaryLogClient client;

  private BinlogPositionHandler positionHandler;

  private RedissonClient redissonClient;

  private final Map<String, BinlogEventHandlerInvoker<T>> eventHandlerList = new HashMap<>();

  private final ExecutorService executor;

  public BinlogClient(BinlogClientConfig clientConfig) {
    this.createPositionHandler(clientConfig);
    this.createRedissonClient(clientConfig);
    this.clientConfig = clientConfig;
    this.executor = Executors.newCachedThreadPool();
  }

  @Override
  public void registerEventHandler(String handlerKey, IBinlogEventHandler<T> eventHandler) {
    BinlogEventHandlerInvoker<T> eventHandlerDetails = new BinlogEventHandlerInvoker<>();
    eventHandlerDetails.setClientConfig(clientConfig);
    eventHandlerDetails.setEventHandler(eventHandler);
    this.eventHandlerList.put(handlerKey, eventHandlerDetails);
  }

  @Override
  public void registerEventHandler(IBinlogEventHandler<T> eventHandler) {
    this.registerEventHandler(UUID.randomUUID().toString(), eventHandler);
  }

  @Override
  public void unregisterEventHandler(String handlerKey) {
    eventHandlerList.remove(handlerKey);
  }

  @Override
  public void connect() {
    // 使用私有方法封装连接逻辑
    try {
      if (clientConfig.getMode() == BinlogClientMode.CLUSTER) {
        connectWithCluster();
      } else {
        connectWithStandalone();
      }
    } catch (Exception e) {
      throw new Binlog4jException(e);
    }
  }

  private void connectWithStandalone() {
    // 提交任务到executor，并处理可能的异常
    try {
      executor.submit(this::runWithStandalone);
    } catch (RejectedExecutionException e) {
      executor.shutdownNow(); // 关闭线程池
      throw new Binlog4jException(e);
    }
  }

  private void connectWithCluster() {
    ScheduledExecutorService scheduledExecutor = Executors.newScheduledThreadPool(1);
    try {
      scheduledExecutor.scheduleWithFixedDelay(this::runWithCluster, 0, 1000,
          TimeUnit.MILLISECONDS);
    } catch (Exception e) {
      scheduledExecutor.shutdown();
      throw new Binlog4jException(e);
    }
  }

  @Override
  public void disconnect() {
    if (client != null) {
      try {
        client.disconnect();
      } catch (Exception e) {
        throw new Binlog4jException(e);
      }
    }
  }

  public void runWithStandalone() {
    try {
      log.info("启动 Binlog 客户端 ({}) - 连接 {}:{} 服务", clientConfig.getServerId(),
          clientConfig.getHost(), clientConfig.getPort());
      client = new BinaryLogClient(clientConfig.getHost(), clientConfig.getPort(),
          clientConfig.getUsername(), clientConfig.getPassword());
      client.registerEventListener(
          new BinlogEventDispatcher<>(this.clientConfig, positionHandler, this.eventHandlerList));
      client.setKeepAlive(clientConfig.isKeepAlive());
      client.setKeepAliveInterval(clientConfig.getKeepAliveInterval());
      client.setHeartbeatInterval(clientConfig.getHeartbeatInterval());
      client.setConnectTimeout(clientConfig.getConnectTimeout());
      client.setServerId(clientConfig.getServerId());
      if (clientConfig.isInaugural() && clientConfig.getBinlogFilename() != null
          && clientConfig.getBinlogPosition() != null) {
        client.setBinlogPosition(clientConfig.getBinlogPosition());
        client.setBinlogFilename(clientConfig.getBinlogFilename());
      }
      if (clientConfig.isPersistence() && (!clientConfig.isInaugural()) && (positionHandler
          != null)) {
        BinlogPosition binlogPosition = positionHandler.loadPosition(
            clientConfig.getServerId());
        if (binlogPosition != null) {
          client.setBinlogFilename(binlogPosition.getFilename());
          client.setBinlogPosition(binlogPosition.getPosition());
        }
      }
      client.connect();
    } catch (Exception e) {
      throw new Binlog4jException(e);
    }
  }

  public void runWithCluster() {
    RLock lock = redissonClient.getLock(clientConfig.getKey());
    try {
      if (lock.tryLock()) {
        runWithStandalone();
        lock.unlock();
      }
    } finally {
      if (lock.isHeldByCurrentThread()) {
        lock.unlock();
      }
    }
  }

  private void createPositionHandler(BinlogClientConfig clientConfig) {
    if (clientConfig.isPersistence()) {
      if (clientConfig.getPositionHandler() == null) {
        if (clientConfig.getRedisConfig() == null) {
          throw new Binlog4jException(
              "Cluster mode or persistence enabled, missing Redis configuration");
        } else {
          this.positionHandler = new RedisBinlogPositionHandler(clientConfig.getRedisConfig());
        }
      } else {
        this.positionHandler = clientConfig.getPositionHandler();
      }
    }
  }

  private void createRedissonClient(BinlogClientConfig clientConfig) {
    if (clientConfig.getMode() == BinlogClientMode.CLUSTER) {
      RedisConfig redisConfig = clientConfig.getRedisConfig();
      if (redisConfig == null) {
        throw new Binlog4jException(
            "Cluster mode or persistence enabled, missing Redis configuration");
      }
      Config config = new Config();
      SingleServerConfig singleServerConfig = config.useSingleServer();
      singleServerConfig.setAddress(
          "redis://" + redisConfig.getHost() + ":" + redisConfig.getPort());
      singleServerConfig.setPassword(redisConfig.getPassword());
      singleServerConfig.setDatabase(redisConfig.getDatabase());
      config.setLockWatchdogTimeout(10000L);
      this.redissonClient = Redisson.create(config);
    }
  }
}
