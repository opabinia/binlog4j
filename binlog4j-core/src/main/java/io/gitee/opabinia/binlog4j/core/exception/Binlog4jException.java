package io.gitee.opabinia.binlog4j.core.exception;

public class Binlog4jException extends RuntimeException {

  public Binlog4jException(String message) {
    super(message);
  }

  public Binlog4jException(String message, Throwable cause) {
    super(message);
  }

  public Binlog4jException(Throwable e) {
    super(e);
  }
}
